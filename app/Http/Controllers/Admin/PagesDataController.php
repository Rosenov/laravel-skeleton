<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesDataController extends Controller
{
    protected function getView()   {
        return view('admin/pages/all-page-data', ['posts' => $this->getAllPageData()]);
    }

    protected function getAllPageData() {
        return Page::all();
    }

    protected function getPageData($id) {
        return Page::where(array('id' => $id))->get()->first();
    }

    protected function editPage($id, Request $request) {
        if($request->isMethod('post')) {
            $page = $this->getPageData($id);
            $page->title = $request->input('title');
            $page->description = $request->input('description');
            $page->keywords = $request->input('keywords');
            $page->social_title = $request->input('social_title');
            $page->social_description = $request->input('social_description');
            $page->media_id = $request->input('image');
            $page->html = $request->input('html-section');

            $page->save();
            return view('admin/pages/edit-page', ['success' => ['Page was edited successfully.'], 'post' => $page]);
        }else {
            return view('admin/pages/edit-page', ['post' => $this->getPageData($id)]);
        }
    }
}