@extends("admin.layout")
@section("content")
    <section class="content edit-page">
        <h1>Edit page</h1>
        <form method="post" enctype="multipart/form-data" class="text-center" action="{{route('edit-page', ['id' => $post->id])}}">
            <div class="form-row">
                <label>Meta title <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Name of the page, appears in the browser tab and in google search results. Maximum length 60 symbols."></i>:</label>
                <input type="text" name="title" value="{{$post->title}}" />
            </div>
            <div class="form-row">
                <label>Meta description <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Description of the page, appears in google search results. Maximum length 140 symbols."></i>:</label>
                <input type="text" name="description" value="{{$post->description}}" />
            </div>
            <div class="form-row">
                <label>Meta keywords <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="No maximum length, keywords are listed with comma separator between them."></i>:</label>
                <input type="text" name="keywords" value="{{$post->keywords}}" />
            </div>
            <div class="form-row">
                <label>Social title <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Social title that appears in facebook when sharing the page. Maximum length 40 symbols."></i>:</label>
                <input type="text" name="social_title" value="{{$post->social_title}}" />
            </div>
            <div class="form-row">
                <label>Social description <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Social description that appears in facebook when sharing the page. Maximum length 300 symbols."></i>:</label>
                <input type="text" name="social_description" value="{{$post->social_description}}" />
            </div>
            <div class="btn-container text-left media padding-bottom-20" @if(!empty($post)) data-id="{{$post->id}}" @endif>
                <label>Social image <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Social image that appears in facebook when sharing the page. Recommended size is 1200x630px."></i>:</label>
                <figure class="image-visualization">
                    @if(!empty($post) && !empty($post->media))
                        <img class="small-image" src="{{URL::asset('assets/uploads/'.$post->media->name) }}"/>
                    @endif
                </figure>
                <a @if(!empty($post)) href="javascript:openMedia({{$post->id}}, true, 'image')" @else  href="javascript:openMedia(null, true, 'image')" @endif class="btn">Select image</a>
                <input type="hidden" name="image" class="hidden-input-image" value="@if(!empty($post->media)) {{$post->media->id}} @endif">
            </div>
            <div class="form-row">
                <label>HTML:</label>
                <textarea class="ckeditor-init" id="html-section" name="html-section">{{$post->html}}</textarea>
            </div>
            <div class="btn-container">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <button class="btn">Save</button>
            </div>
        </form>
    </section>
@endsection